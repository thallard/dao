project(daov)

set(EOSIO_WASM_OLD_BEHAVIOR "Off")
find_package(eosio.cdt)

add_contract( daov daov daov.cpp)
target_include_directories( daov PUBLIC ${CMAKE_SOURCE_DIR}/../include )
target_ricardian_directory( daov ${CMAKE_SOURCE_DIR}/../ricardian )