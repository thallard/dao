import { defineStore } from 'pinia';
import { ApiStore } from './api';
import { RefreshStore } from './refresh';

export const BoardStore = defineStore('board', {
    state: () => ({
        api: null,
    }),
    actions: {
        // Refresh the API value 
        initializeBoard() {
            const store = ApiStore();

            this.api = store.getApi();
        },
        async makeChoice(name, choice) {
            this.initializeBoard();
            const store = RefreshStore();

            console.log(store.getActionChoices)
            try {
                result = await this.api.transact(
                    {
                        actions: [
                            {
                                account: "daoventuresc",
                                name: "doaction",
                                authorization: [
                                    {
                                        actor: name,
                                        permission: "active",
                                    },
                                ],
                                data: { owner: name, choice: choice },
                            },
                        ],
                    },
                    {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    }
                );
                // console.log(result);
                // return result;
            } catch (error) {
                alert("Error :" , error);
            }
            var result = "";
            let actions = store.getActionChoices;
            console.log(actions[choice])
            switch (actions[choice]) {
                case "PAY":
                    result = "You paid ";
                    break;
                default:
                    break;
            }
            return result;
        },
        // Call the SC and get the number
        async rollDice(name) {
            this.initializeBoard();
         
            // Call launch function from smart contract
            try {
                const result = await this.api.transact(
                    {
                        actions: [
                            {
                                account: "daoventuresc",
                                name: "launch",
                                authorization: [
                                    {
                                        actor: name,
                                        permission: "active",
                                    },
                                ],
                                data: { owner: name },
                            },
                        ],
                    },
                    {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    }
                );
                return result;
            } catch (error) {
                console.log("Error :", error);
                return 1;
            }
            // return null;
        }
    }
});