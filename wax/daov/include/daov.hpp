#pragma once
#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/crypto.hpp>
#include <eosio/system.hpp>
#include <vector>
#include <map>

using namespace eosio;
using namespace std;

typedef struct s_action {
				string name;
				asset price;
			}				t_action;

CONTRACT daov : public contract {
	public:
		using contract::contract;
		daov(name receiver, name code, datastream<const char*> ds): 
		contract(receiver, code, ds), 
		accounts(receiver, receiver.value), 
		rand(receiver, receiver.value), 
		conf(receiver, receiver.value),
		tools(receiver, receiver.value),
		lands(receiver, receiver.value),
		buildings(receiver, receiver.value),
		wheel(receiver, receiver.value),
		temples(receiver, receiver.value),
		map(receiver, receiver.value),
		contribs(receiver, receiver.value),
		boosts(receiver, receiver.value),
		energies(receiver, receiver.value),
		companies(receiver, receiver.value),
		uactions(receiver, receiver.value),
		bank(receiver, receiver.value),
		neverlucky(receiver, receiver.value),
		quizz(receiver, receiver.value)
		{}

		ACTION hi( name owner);
		using hi_action = action_wrapper<"hi"_n, &daov::hi>;

		ACTION login( name owner);
		using login_action = action_wrapper<"login"_n, &daov::login>;

		void newuser( name owner );
		// using register_action = action_wrapper<"newuser"_n, &daov::newuser>;

		ACTION rmuser( name account );
		using rmuser_action = action_wrapper<"rmuser"_n, &daov::rmuser>;

		ACTION launch( name owner );
		using launch_action = action_wrapper<"launch"_n, &daov::launch>;

		ACTION gotocase(name owner, uint16_t n);
		using gotocase_action = action_wrapper<"gotocase"_n, &daov::gotocase>;

		ACTION editconf( uint64_t id , uint64_t next_claim_s, uint8_t max_launch, asset exit_jail_price, asset reward_start);
		using editconf_action = action_wrapper<"editconf"_n, &daov::editconf>;
		
		ACTION rmconf( uint64_t id);
		using rmconf_action = action_wrapper<"rmconf"_n, &daov::rmconf>;

		ACTION rmaction(name owner, uint64_t id);
		using rmaction_action = action_wrapper<"rmaction"_n, &daov::rmaction>;

		ACTION rmfaction(name owner);
		using rmfaction_action = action_wrapper<"rmfaction"_n, &daov::rmfaction>;
		

		ACTION initrandom(name owner, uint64_t customer_id, uint64_t signing_value);
		ACTION getrandom(name owner, uint64_t customer_id);
		ACTION receiverand(uint64_t customer_id, const eosio::checksum256& random_value);
		using initrandom_action = action_wrapper<"initrandom"_n, &daov::initrandom>;
		using getrandom_action = action_wrapper<"getrandom"_n, &daov::getrandom>;
		using receiverand_action = action_wrapper<"receiverand"_n, & daov::receiverand>;

		// ACTION settool(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img);
		// using settool_action = action_wrapper<"settool"_n, &daov::settool>;
		// ACTION rmtool(uint64_t id);
		// using rmtool_action = action_wrapper<"rmtool"_n, &daov::rmtool>;
		// ACTION settoolimg(uint64_t id, string img);
		// using settoolimg_action = action_wrapper<"settoolimg"_n, &daov::settoolimg>;

		// ACTION setland(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img, vector<eosio::name> owners);
		// using setland_action = action_wrapper<"setland"_n, &daov::setland>;
		// ACTION rmland(uint64_t id);
		// using rmland_action = action_wrapper<"rmland"_n, &daov::rmland>;
		// ACTION setlandimg(uint64_t id, string img);
		// using setlandimg_action = action_wrapper<"setlandimg"_n, &daov::setlandimg>;

		// ACTION setbuilding(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img, vector<eosio::name> owners);
		// using setbuilding_action = action_wrapper<"setbuilding"_n, &daov::setbuilding>;
		// ACTION rmbuilding(uint64_t id);
		// using rmbuilding_action = action_wrapper<"rmbuilding"_n, &daov::rmbuilding>;
		// ACTION setbldimg(uint64_t id, string img);
		// using setbldimg_action = action_wrapper<"setbldimg"_n, &daov::setbldimg>;

		// ACTION setenergy(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img, vector<eosio::name> owners);
		// using setenergy_action = action_wrapper<"setenergy"_n, &daov::setenergy>;
		// ACTION rmenergy(uint64_t id);
		// using rmenergy_action = action_wrapper<"rmenergy"_n, &daov::rmenergy>;
		// ACTION setenergyimg(uint64_t id, string img);
		// using setenergyimg_action = action_wrapper<"setenergyimg"_n, &daov::setenergyimg>;

		// ACTION setcompany(uint64_t id, string type, string name, asset price, vector<asset> reward, string img, vector<eosio::name> owners);
		// using setcompany_action = action_wrapper<"setcompany"_n, &daov::setcompany>;
		// ACTION rmcompany(uint64_t id);
		// using rmcompany_action = action_wrapper<"rmcompany"_n, &daov::rmcompany>;
		// ACTION setcompanyimg(uint64_t id, string img);
		// using setcompanyimg_action = action_wrapper<"setcompanyimg"_n, &daov::setcompanyimg>;


		// ACTION setwheel(uint64_t id, string type, asset value);
		// using setwheel_action = action_wrapper<"setwheel"_n, &daov::setwheel>;
		// ACTION rmwheel(uint64_t id);
		// using rmwheel_action = action_wrapper<"rmwheel"_n, &daov::rmwheel>;

		// ACTION settemple(uint64_t id, string type, string name, uint8_t level, asset reward, string img);
		// using settemple_action = action_wrapper<"settemple"_n, &daov::settemple>;
		// ACTION rmtemple(uint64_t id);
		// using rmtemple_action = action_wrapper<"rmtemple"_n, &daov::rmtemple>;
		// ACTION settempleimg(uint64_t id, string img);
		// using settempleimg_action = action_wrapper<"settempleimg"_n, &daov::settempleimg>;

		// ACTION setcase(uint64_t id, string case_type, uint64_t type_id);
		// using setcase_action = action_wrapper<"setcase"_n, &daov::setcase>;
		// ACTION rmcase(uint64_t id);
		// using rmcase_action = action_wrapper<"rmcase"_n, &daov::rmcase>;

		// ACTION setcontrib(uint64_t id, string type, asset cost, string img);
		// using setcontrib_action = action_wrapper<"setcontrib"_n, &daov::setcontrib>;
		// ACTION rmcontrib(uint64_t id);
		// using rmcontrib_action = action_wrapper<"rmcontrib"_n, &daov::rmcontrib>;
		// ACTION setcontribimg(uint64_t id, string img);
		// using setcontribimg_action = action_wrapper<"setcontribimg"_n, &daov::setcontribimg>;
		
		// ACTION setboost(uint64_t id, string type, string name,vector<uint64_t> values,asset price, string img);
		// using setboost_action = action_wrapper<"setboost"_n, &daov::setboost>;
		// ACTION rmboost(uint64_t id);
		// using rmboost_action = action_wrapper<"rmboost"_n, &daov::rmboost>;
		// ACTION setboostimg(uint64_t id, string img);
		// using setboostimg_action = action_wrapper<"setboostimg"_n, &daov::setboostimg>;

		ACTION rmactionuser(name owner);
		using rmactionuser_action = action_wrapper<"rmactionuser"_n, &daov::rmactionuser>;

		// ACTION givedaov(name owner, asset ammount);
		// using givedaov_action = action_wrapper<"givedaov"_n, &daov::givedaov>;

		// ACTION setnl(uint64_t id, string desc, asset reward, int16_t move);
		// using setnl_action = action_wrapper<"setnl"_n, &daov::setnl>;

		ACTION setquizz(uint64_t id, string question, vector<string> answers, checksum256 valid);
		using setquizz_action = action_wrapper<"setquizz"_n, &daov::setquizz>;

		void testquizz(name owner, uint64_t id, int8_t answer);
		
		void move(name owner, uint8_t one, uint8_t two, uint64_t act_id);
		void onLand(name owner, bool pay);
		void onQuizz(name owner);
		void onContribs(name owner);
		void onEnergy(name owner, bool pay);
		void onBuilding(name owner, bool pay);
		void launchWheel(name owner);
		void onWheel(name owner);
		void onTool(name owner);
		void onBoost(name owner);
		void onSwitch(name owner);
		void onNeverlucky(name owner);
		void onCompany(name owner);
		void getcase(name owner, bool pay);
		void onTemple(name owner, uint64_t temple_id);

		void handleBuy(name owner, string type, uint64_t id);
		void buyLand(name owner, uint64_t id);
		void buyEnergy(name owner, uint64_t id);
		void buyCompany(name owner, uint64_t id);
		void buyBuilding(name owner, uint64_t id);

		uint64_t getNbOfBoost(name owner, string type);

		ACTION addlaunch(name owner, uint8_t n);
		using addlaunch_action = action_wrapper<"addlaunch"_n, &daov::addlaunch>;

		ACTION doaction(name owner, uint8_t choice);
		using doaction_action = action_wrapper<"doaction"_n, &daov::doaction>;

	private:
			TABLE accounts_table {
				name	account;
				vector<asset>		balances;
				uint64_t	last_claim;
				uint8_t		launch;
				vector<uint8_t>  last_launch;
				uint16_t	position;
				uint8_t		jailed;
				uint64_t    action_id;
				vector<uint64_t> tools;
				vector<uint64_t> boosts;

				uint64_t primary_key() const { return account.value; }
			};
			typedef multi_index< "accounts"_n, accounts_table> accounts_t;
			accounts_t accounts;

			TABLE rand_table {
				uint64_t	id;
				checksum256	number;

				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"rand"_n, rand_table> rand_t;
			rand_t rand;

			TABLE conf_table {
				uint64_t	id;
				uint64_t	next_claim_s;
				uint8_t		max_launch;
				asset		exit_jail_price;
				asset		reward_start;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"config"_n, conf_table> conf_t;
			conf_t conf;

			TABLE tools_conf {
				uint64_t id;
				string	type;
				string	name;
				uint8_t	level;
				asset	price;
				asset	reward;
				string	img;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"tools"_n, tools_conf> tools_t;
			tools_t tools;

			TABLE lands_conf {
				uint64_t id;
				string type;
				string name;
				uint8_t level;
				asset	price;
				asset	reward;
				string	img;
				vector<eosio::name> owners;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"lands"_n, lands_conf> lands_t;
			typedef multi_index<"buildings"_n, lands_conf> buildings_t;
			typedef multi_index<"energies"_n, lands_conf> energies_t;
			energies_t energies;
			buildings_t buildings;
			lands_t lands;

			TABLE companies_conf {
				uint64_t id;
				string type;
				string name;
				asset	price;
				vector<asset>	reward;
				string	img;
				vector<eosio::name> owners;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"companies"_n, companies_conf> companies_t;
			companies_t companies;

			TABLE wheel_conf {
				uint64_t	id;
				string		type;
				asset		value;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"wheel"_n, wheel_conf> wheel_t;
			wheel_t wheel;

			TABLE temples_conf {
				uint64_t id;
				string type;
				string name;
				uint8_t level;
				asset reward;
				string img;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"temples"_n, temples_conf> temples_t;
			temples_t temples;

			TABLE map_conf {
				uint64_t id;
				string case_type;
				uint64_t type_id;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"map"_n, map_conf> map_t;
			map_t map;

			TABLE contrib_conf {
				uint64_t id;
				string type;
				asset cost;
				string img;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"contribs"_n, contrib_conf> contribs_t;
			contribs_t contribs;

			TABLE boost_conf {
				uint64_t id;
				string type;
				string name;
				vector<uint64_t> values;
				asset price;
				string img;
			uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"boosts"_n, boost_conf> boosts_t;
			boosts_t boosts;

			TABLE actions_table {
				uint64_t id;
				name owner;
				uint16_t map_id;
				bool			completed;
				vector<string>	action_name;
				vector<asset>	action_price;
				vector<name>	player_to_pay;
				vector<asset>	price_for_each;
				uint64_t primary_key()const { return id;};
				uint64_t getOwner()const {return owner.value;};
			};
			typedef multi_index<"uactions"_n, actions_table, indexed_by<"getowner"_n, const_mem_fun<actions_table, uint64_t,&actions_table::getOwner>>> actions_t;
			actions_t uactions;

			TABLE bank_table {
				uint64_t id;
				asset giveaway;
				uint64_t primary_key()const { return id;};
			};

			typedef multi_index<"bank"_n, bank_table> bank_t;
			bank_t bank;

			TABLE neverlucky_table {
				uint64_t id;
				string desc;
				asset reward;
				int16_t move;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"neverlucky"_n, neverlucky_table> neverlucky_t;
			neverlucky_t neverlucky;

			TABLE quizz_table {
				uint64_t id;
				string question;
				vector<string> answers;
				checksum256	valid;
				uint64_t primary_key()const { return id;};
			};
			typedef multi_index<"quizz"_n, quizz_table> quizz_t;
			quizz_t quizz;

			uint8_t get_dice(bool first);
};