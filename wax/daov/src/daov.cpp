#include <daov.hpp>

int getBoostType(string type)
{
   string list[4] = {"tools", "lands", "energies", "buildings"};
   for (int i = 0; i < 4; i++)
   {
      if (type == list[i])
         return i;
   }

   return -1;
}

constexpr unsigned int str2int(const char *str, int h = 0)
{
   return !str[h] ? 5381 : (str2int(str, h + 1) * 33) ^ str[h];
}

ACTION daov::hi(name nm)
{
   /* fill in action body */
   print_f("Name : %\n", nm);
}

ACTION daov::login(name owner)
{
   require_auth(owner);

   auto user = accounts.find(owner.value);
   if (user == accounts.end())
   {
      newuser(owner);
      return;
   }
   auto config = conf.begin();

   uint64_t last_claim = user->last_claim;

   if (user->last_claim + config->next_claim_s <= current_time_point().sec_since_epoch() && user->launch < config->max_launch)
   {
      accounts.modify(accounts.find(owner.value), owner, [&](auto &rec)
                      {
                         rec.launch += 1;
                         rec.last_claim = current_time_point().sec_since_epoch(); });
   }
   if (user->last_claim + (config->next_claim_s * 2) <= current_time_point().sec_since_epoch() && user->launch < config->max_launch)
   {
      accounts.modify(accounts.find(owner.value), owner, [&](auto &rec)
                      {
                         rec.launch += 1;
                         rec.last_claim = current_time_point().sec_since_epoch(); });
   }
}

void daov::newuser(name owner)
{

   require_auth(owner);

   auto user = accounts.find(owner.value);

   check(user == accounts.end(), "User already exist, please login!");

   accounts.emplace(owner, [&](auto &rec)
                    {
                       rec.account = owner;
                       rec.balances.push_back(asset(1000, symbol("DAOV", 0)));
                       rec.balances.push_back(asset(15000, symbol("PERC", 3)));
                       rec.last_claim = current_time_point().sec_since_epoch();
                       rec.launch = 1;
                       rec.last_launch.push_back(0);
                       rec.last_launch.push_back(0);
                       rec.position = 1;
                       rec.jailed = 0;
                       for (int i = 0; i < 4; i++)
                          rec.boosts.push_back(0); });
}

ACTION daov::rmuser(name account)
{
   require_auth(_self);

   auto user = accounts.find(account.value);
   check(user != accounts.end(), "User already exist, please login!");

   accounts.erase(user);
}

/*
 *                  BOOSTS
 */

// ACTION daov::setboost(uint64_t id, string type, string name, vector<uint64_t> values, asset price, string img)
// {
//    require_auth(_self);

//    auto boost = boosts.find(id);

//    if (boost != boosts.end())
//    {
//       boosts.modify(boost, _self, [&](auto &rec)
//                     {
//                        rec.id = id;
//                        rec.type = type;
//                        rec.name = name;
//                        rec.values = values;
//                        rec.price = price;
//                        rec.img = img; });
//    }
//    else
//    {
//       boosts.emplace(_self, [&](auto &rec)
//                      {
//                         rec.id = id;
//                         rec.type = type;
//                         rec.name = name;
//                         rec.values = values;
//                         rec.price = price;
//                         rec.img = img; });
//    }
// }
// ACTION daov::rmboost(uint64_t id)
// {
//    require_auth(_self);

//    auto boost = boosts.get(id, "No land with this id");

//    boosts.erase(boosts.find(id));
// }

// ACTION daov::setboostimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto boost = boosts.find(id);

//    check(boost != boosts.end(), "No boost with this id");

//    boosts.modify(boost, _self, [&](auto &rec)
//                  { rec.img = img; });
// }

// ACTION daov::setcontrib(uint64_t id, string type, asset cost, string img)
// {
//    require_auth(_self);

//    auto contrib = contribs.find(id);

//    if (contrib != contribs.end())
//    {
//       contribs.modify(contrib, _self, [&](auto &rec)
//                       {
//                          rec.id = id;
//                          rec.type = type;
//                          rec.cost = cost;
//                          rec.img = img; });
//    }
//    else
//    {
//       contribs.emplace(_self, [&](auto &rec)
//                        {
//                           rec.id = id;
//                           rec.type = type;
//                           rec.cost = cost;
//                           rec.img = img; });
//    }
// }
// ACTION daov::rmcontrib(uint64_t id)
// {
//    require_auth(_self);

//    auto contrib = contribs.get(id, "No land with this id");

//    contribs.erase(contribs.find(id));
// }

// ACTION daov::setcontribimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto contrib = contribs.find(id);

//    check(contrib != contribs.end(), "No contrib with this id");

//    contribs.modify(contrib, _self, [&](auto &rec)
//                    { rec.img = img; });
// }

/*========================================================================================================================
* ========================================================================================================================
*                  xBUILDINGS
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setbuilding(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img, vector<eosio::name> owners)
// {
//    require_auth(_self);

//    auto building = buildings.find(id);

//    if (building != buildings.end())
//    {
//       buildings.modify(building, _self, [&](auto &rec)
//                        {
//                           rec.id = id;
//                           rec.type = type;
//                           rec.name = name;
//                           rec.level = level;
//                           rec.price = price;
//                           rec.reward = reward;
//                           rec.img = img;
//                           rec.owners = owners; });
//    }
//    else
//    {
//       buildings.emplace(_self, [&](auto &rec)
//                         {
//                            rec.id = id;
//                            rec.type = type;
//                            rec.name = name;
//                            rec.level = level;
//                            rec.price = price;
//                            rec.reward = reward;
//                            rec.img = img;
//                            rec.owners = owners; });
//    }
// }

// ACTION daov::rmbuilding(uint64_t id)
// {
//    require_auth(_self);

//    auto building = buildings.get(id, "No building with this id");

//    buildings.erase(buildings.find(id));
// }

// ACTION daov::setbldimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto building = buildings.find(id);

//    check(building != buildings.end(), "No building with this id");

//    buildings.modify(building, _self, [&](auto &rec)
//                     { rec.img = img; });
// }

void daov::onBuilding(name owner, bool pay)
{
   // auto list_type = [&tools, &lands, &energies, &buildings];

   auto user = accounts.find(owner.value);

   auto building = buildings.find(map.find(user->position)->type_id);

   uint64_t nb_boost = 0;
   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   asset cost = asset(0, symbol("DAOV", 0));

   bool owned = false;

   for (auto it = building->owners.begin(); it != building->owners.end(); it++)
   {
      auto member = accounts.find((*it).value);
      if (member->account == owner)
      {
         owned = true;
         continue;
      }
      nb_boost += member->boosts[3] ? member->boosts[3] - 1 : 0;
   }

   asset base = asset(building->owners.size() ? building->reward.amount / building->owners.size() : 0, symbol("DAOV", 0));

   if (pay && building->owners.size())
   {
      if (building->owners.size() == 1 && building->owners.begin()->value == user->account.value)
         return;
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = user->position;
         rec.action_name.push_back("PAY");
         


         for(auto it = building->owners.begin(); it != building->owners.end(); it++) {
            auto member = accounts.find((*it).value);
            if (member == user) continue;
            rec.player_to_pay.push_back(member->account);
            if (!member->boosts[3]) {
               cost += base;
               rec.price_for_each.push_back(base);
            }
            else {
               asset price = base;
               price.amount += base.amount / nb_boost * (member->boosts[3]) ? (boosts.find(3)->values[member->boosts[3] - 2 ] / 100) : 1;
               cost += price;
               rec.price_for_each.push_back(price);
            }
         }

         rec.action_price.push_back(cost); });
      accounts.modify(user, owner, [&](auto &rec)
                      { rec.action_id = act_id; });
   }
   else
   {
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.owner = owner;
         rec.completed = false;
         rec.map_id = user->position;
         if (!owned){
         rec.action_name.push_back("BUY");
         rec.action_price.push_back(building->price);
                       }
         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
      if (!owned)
      {
         accounts.modify(user, owner, [&](auto &rec)
                         { rec.action_id = act_id; });
      }
   }
}

/*========================================================================================================================
* ========================================================================================================================
*                  xCOMPANY
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setcompany(uint64_t id, string type, string name, asset price, vector<asset> reward, string img, vector<eosio::name> owners)
// {
//    require_auth(_self);

//    auto company = companies.find(id);

//    if (company != companies.end())
//    {
//       companies.modify(company, _self, [&](auto &rec)
//                        {
//                           rec.id = id;
//                           rec.type = type;
//                           rec.name = name;
//                           rec.price = price;
//                           rec.reward = reward;
//                           rec.img = img;
//                           rec.owners = owners; });
//    }
//    else
//    {
//       companies.emplace(_self, [&](auto &rec)
//                         {
//                            rec.id = id;
//                            rec.type = type;
//                            rec.name = name;
//                            rec.price = price;
//                            rec.reward = reward;
//                            rec.img = img;
//                            rec.owners = owners; });
//    }
// }

// ACTION daov::rmcompany(uint64_t id)
// {
//    require_auth(_self);

//    auto company = companies.get(id, "No company with this id");

//    companies.erase(companies.find(id));
// }

// ACTION daov::setcompanyimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto company = companies.find(id);

//    check(company != companies.end(), "No company with this id");

//    companies.modify(company, _self, [&](auto &rec)
//                     { rec.img = img; });
// }

/*========================================================================================================================
* ========================================================================================================================
*                  xCONFIG
* ========================================================================================================================
  =======================================================================================================================*/

ACTION daov::editconf(uint64_t id, uint64_t next_claim_s, uint8_t max_launch, asset exit_jail_price, asset reward_start)
{
   require_auth(_self);

   auto config = conf.find(id);

   if (config != conf.end())
   {
      conf.modify(config, _self, [&](auto &rec)
                  {
                     rec.id = id;
                     rec.next_claim_s = next_claim_s;
                     rec.max_launch = max_launch;
                     rec.exit_jail_price = exit_jail_price;
                     rec.reward_start = reward_start; });
   }
   else
   {
      conf.emplace(_self, [&](auto &rec)
                   {
                      rec.id = id;
                      rec.next_claim_s = next_claim_s;
                      rec.max_launch = max_launch;
                      rec.exit_jail_price = exit_jail_price;
                      rec.reward_start = reward_start; });
   }
}

ACTION daov::rmconf(uint64_t id)
{
   require_auth(_self);

   auto config = conf.get(id, "No config with this id");

   conf.erase(conf.find(id));
}

/*========================================================================================================================
* ========================================================================================================================
*                  xENERGY
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setenergy(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img, vector<eosio::name> owners)
// {
//    require_auth(_self);

//    auto energy = energies.find(id);

//    if (energy != energies.end())
//    {
//       energies.modify(energy, _self, [&](auto &rec)
//                       {
//                          rec.id = id;
//                          rec.type = type;
//                          rec.name = name;
//                          rec.level = level;
//                          rec.price = price;
//                          rec.reward = reward;
//                          rec.img = img;
//                          rec.owners = owners; });
//    }
//    else
//    {
//       energies.emplace(_self, [&](auto &rec)
//                        {
//                           rec.id = id;
//                           rec.type = type;
//                           rec.name = name;
//                           rec.level = level;
//                           rec.price = price;
//                           rec.reward = reward;
//                           rec.img = img;
//                           rec.owners = owners; });
//    }
// }

// ACTION daov::rmenergy(uint64_t id)
// {
//    require_auth(_self);

//    auto energy = energies.get(id, "No energy with this id");

//    energies.erase(energies.find(id));
// }

// ACTION daov::setenergyimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto energy = energies.find(id);

//    check(energy != energies.end(), "No energy with this id");

//    energies.modify(energy, _self, [&](auto &rec)
//                    { rec.img = img; });
// }

void daov::onEnergy(name owner, bool pay)
{

   auto user = accounts.find(owner.value);

   auto energy = energies.find(map.find(user->position)->type_id);

   uint64_t nb_boost = 0;

   asset cost = asset(0, symbol("DAOV", 0));
   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   bool owned = false;

   for (auto it = energy->owners.begin(); it != energy->owners.end(); it++)
   {
      auto member = accounts.find((*it).value);
      if (member->account == owner)
      {
         owned = true;
         continue;
      }
      nb_boost += member->boosts[2] ? member->boosts[2] - 1 : 0;
   }

   asset base = asset(energy->owners.size() ? energy->reward.amount / energy->owners.size() : 0, symbol("DAOV", 0));

   if (pay && energy->owners.size())
   {

      if (energy->owners.size() == 1 && energy->owners.begin()->value == user->account.value)
         return;
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = user->position;
         rec.action_name.push_back("PAY");
         
         


         for(auto it = energy->owners.begin(); it != energy->owners.end(); it++) {
            auto member = accounts.find((*it).value);
            if (member == user) continue;
            rec.player_to_pay.push_back(member->account);
            if (!member->boosts[2]) {
               cost += base;
               rec.price_for_each.push_back(base);
            }
            else {
               asset price = base;
               price.amount += base.amount / nb_boost * (member->boosts[2]) ? (boosts.find(2)->values[member->boosts[2] - 2 ] / 100) : 1;
               cost += price;
               rec.price_for_each.push_back(price);
            }
         }

         rec.action_price.push_back(cost); });
      accounts.modify(user, owner, [&](auto &rec)
                      { rec.action_id = act_id; });
   }
   else
   {

      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = user->position;
         if (!owned){
         rec.action_name.push_back("BUY");
         rec.action_price.push_back(energy->price);
         }
         else {
            rec.completed = true;
         }
         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });

      if (!owned)
      {
         accounts.modify(user, owner, [&](auto &rec)
                         { rec.action_id = act_id; });
      }
   }
}

/*========================================================================================================================
* ========================================================================================================================
*                  xLANDS
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setland(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img, vector<eosio::name> owners)
// {
//    require_auth(_self);

//    auto land = lands.find(id);

//    if (land != lands.end())
//    {
//       lands.modify(land, _self, [&](auto &rec)
//                    {
//                       rec.id = id;
//                       rec.type = type;
//                       rec.name = name;
//                       rec.level = level;
//                       rec.price = price;
//                       rec.reward = reward;
//                       rec.img = img;
//                       rec.owners = owners; });
//    }
//    else
//    {
//       lands.emplace(_self, [&](auto &rec)
//                     {
//                        rec.id = id;
//                        rec.type = type;
//                        rec.name = name;
//                        rec.level = level;
//                        rec.price = price;
//                        rec.reward = reward;
//                        rec.img = img;
//                        rec.owners = owners; });
//    }
// }

// ACTION daov::rmland(uint64_t id)
// {
//    require_auth(_self);

//    auto land = lands.get(id, "No land with this id");

//    lands.erase(lands.find(id));
// }

// ACTION daov::setlandimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto land = lands.find(id);

//    check(land != lands.end(), "No land with this id");

//    lands.modify(land, _self, [&](auto &rec)
//                 { rec.img = img; });
// }

void daov::onLand(name owner, bool pay)
{

   auto user = accounts.find(owner.value);

   auto land = lands.find(map.find(user->position)->type_id);

   uint64_t nb_boost = 0;

   asset cost = asset(0, symbol("DAOV", 0));
   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   bool owned = false;

   for (auto it = land->owners.begin(); it != land->owners.end(); it++)
   {
      auto member = accounts.find((*it).value);
      if (member->account == owner)
      {
         owned = true;
         continue;
      }
      nb_boost += member->boosts[1] ? member->boosts[1] - 1 : 0;
   }

   asset base = asset(land->owners.size() ? land->reward.amount / land->owners.size() : 0, symbol("DAOV", 0));
   if (pay && land->owners.size())
   {
      if (land->owners.size() == 1 && land->owners.begin()->value == user->account.value)
         return;

      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = user->position;
         rec.action_name.push_back("PAY");
         


         for(auto it = land->owners.begin(); it != land->owners.end(); it++) {
            auto member = accounts.find((*it).value);
            if (member == user)
               continue;
            rec.player_to_pay.push_back(member->account);
            if ( !member->boosts[1]) {
               cost += base;
               rec.price_for_each.push_back(base);
            }
            else {
               asset price = base;
               
                  price.amount += base.amount / nb_boost * (member->boosts[1]) ? (boosts.find(1)->values[member->boosts[1] - 2 ] / 100) : 1;

               cost += price;
               rec.price_for_each.push_back(price);
            }
         }

         rec.action_price.push_back(cost); });
      accounts.modify(user, owner, [&](auto &rec)
                      { rec.action_id = act_id; });
   }
   else
   {
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.owner = owner;
         rec.map_id = user->position;
         if (!owned){
         rec.action_name.push_back("BUY");
         rec.action_price.push_back(land->price);
         }
         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
      if (!owned)
      {
         accounts.modify(user, owner, [&](auto &rec)
                         { rec.action_id = act_id; });
      }
   }
}

/*========================================================================================================================
* ========================================================================================================================
*                  xTMP
* ========================================================================================================================
  =======================================================================================================================*/

ACTION daov::rmaction(name owner, uint64_t id)
{
   require_auth(_self);
   actions_t actions_u(_self, owner.value);
   actions_u.erase(actions_u.find(id));
}

ACTION daov::rmfaction(name owner)
{
   require_auth(_self);
   actions_t actions_u(_self, owner.value);
   auto it = actions_u.begin();
   while (it != actions_u.end())
   {
      actions_u.erase(it);
      it = actions_u.begin();
   }
}

ACTION daov::addlaunch(name owner, uint8_t n)
{
   require_auth(_self);

   auto user = accounts.find(owner.value);

   accounts.modify(user, _self, [&](auto &rec)
                   { rec.launch += n; });
}

/*========================================================================================================================
* ========================================================================================================================
*                  xMAP
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setcase(uint64_t id, string case_type, uint64_t type_id)
// {
//    require_auth(_self);

//    auto map_case = map.find(id);

//    if (map_case != map.end())
//    {
//       map.modify(map_case, _self, [&](auto &rec)
//                  {
//                     rec.id = id;
//                     rec.case_type = case_type;
//                     rec.type_id = type_id; });
//    }
//    else
//    {
//       map.emplace(_self, [&](auto &rec)
//                   {
//                      rec.id = id;
//                      rec.case_type = case_type;
//                      rec.type_id = type_id; });
//    }
// }

// ACTION daov::rmcase(uint64_t id)
// {
//    require_auth(_self);

//    auto map_case = map.get(id, "No land with this id");

//    map.erase(map.find(id));
// }

/*========================================================================================================================
* ========================================================================================================================
*                  xGAME
* ========================================================================================================================
  =======================================================================================================================*/

void daov::move(name owner, uint8_t one, uint8_t two, uint64_t act_id) {
   auto user = accounts.find(owner.value);
   actions_t actions_u(_self, owner.value);
if (!user->jailed)
   {
      accounts.modify(user, owner, [&](auto &rec)
                      {
                         rec.launch--;
                         rec.last_launch[0] = one;
                         rec.last_launch[1] = two;
                         rec.position += one + two;
                         if (rec.position > 40)
                         {
                            rec.position -= 40;
                            rec.balances[0] += conf.begin()->reward_start;
                         }
                         else if (rec.position == 31)
                         {
                            rec.position = 11;
                           rec.action_id = act_id;
                            rec.jailed = 1;

                         }
                         for (auto it = rec.tools.begin(); it != rec.tools.end(); it++) {
                            auto tool = tools.find(*it);
                            auto boost = 100;
                            if (rec.boosts[0])
                              boost += (boosts.find(0)->values[rec.boosts[0] - 1]);
                           rec.balances[0].amount += tool->reward.amount * boost / 100;
                         } });
   }
   else
   {
      if (one == two)
      {
         accounts.modify(user, owner, [&](auto &rec)
                         {
                            rec.launch--;
                            rec.last_launch[0] = one;
                            rec.last_launch[1] = two;
                            rec.position += one + two;
                            rec.jailed = 0;
                            if (rec.position > 40)
                            {
                               rec.position -= 40;
                               rec.balances[0] += conf.begin()->reward_start;
                            }
                            if (rec.position == 31)
                            {
                               rec.position = 11;
                               rec.action_id = act_id;
                               rec.jailed = 1;
                            } });
      }
      else
      {
         accounts.modify(user, owner, [&](auto &rec)
                         {
                            rec.launch--;
                            rec.last_launch[0] = one;
                            rec.last_launch[1] = two;
                            rec.jailed++;
                            if (rec.jailed > 3)
                            {
                               rec.balances[0] -= conf.begin()->exit_jail_price;
                              rec.jailed = 0;
                               if (rec.balances[0].amount < 0)
                                  rec.balances[0] = asset(0, symbol("DAOV", 0));
                            } });
      }
   }
   if (!user->jailed)
      getcase(owner, true);
   else
   {
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = 11;
         rec.action_name.push_back("EXIT");
         rec.action_price.push_back(conf.begin()->exit_jail_price);
         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
   }
}

ACTION daov::gotocase(name owner, uint16_t n)
{
   require_auth(owner);
   getrandom(owner, 77777777777777);
   getrandom(owner, 88888888888888);

   auto user = accounts.find(owner.value);

   check(user != accounts.end(), "No user founded !");
   check(user->action_id == 0, "Pending action...");
   check(user->launch > 0, "No launch left.");

   uint8_t one = n;
   uint8_t two = 0;
   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   move(owner, one, two, act_id);   
}

ACTION daov::launch(name owner)
{
   require_auth(owner);
   getrandom(owner, 77777777777777);
   getrandom(owner, 88888888888888);

   auto user = accounts.find(owner.value);

   check(user != accounts.end(), "No user founded !");
   check(user->action_id == 0, "Pending action...");
   check(user->launch > 0, "No launch left.");

   uint8_t one = get_dice(true);
   uint8_t two = get_dice(false);
   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   move(owner, one, two, act_id);   
}

void daov::getcase(name owner, bool pay)
{
   // (void)case_type;
   // (void)type_id;
   // (void)position;

   // Get info player and check his position
   auto user = accounts.get(owner.value, "Any user exist with this name");
   // check(user.position > 40 || user.position < 0, "The user is out of bounds");

   // Get case info
   auto tile = map.get(user.position, "Case doesn't exist");

   switch (str2int(tile.case_type.c_str()))
   {
   case str2int("lands"):
      onLand(owner, pay);
      break;
   case str2int("buildings"):
      onBuilding(owner, pay);
      break;
   case str2int("energies"):
      onEnergy(owner, pay);
      break;
   case str2int("temples"):
      onTemple(owner, tile.type_id);
      break;
   case str2int("tools"):
      onTool(owner);
      break;
   case str2int("boosts"):
      onBoost(owner);
      break;
   case str2int("switch"):
      onSwitch(owner);
      break;
   case str2int("contribs"):
      onContribs(owner);
      break;
   case str2int("neverlucky"):
      onNeverlucky(owner);
      break;
   case str2int("wheel"):
      onWheel(owner);
      break;
   case str2int("companies"):
      onCompany(owner);
      break;
    case str2int("quizz"):
        onQuizz(owner);
        break;
   default:
      break;
   }

   // TODO: do job on lands, temple, energies or wheel (when my link work)

   // if (case_type == "lands")
   //    ;
}

uint8_t daov::get_dice(bool first)
{
   uint64_t max_value = 6;
   auto &actual = rand.get(first ? 77777777777777 : 88888888888888, "invialid customer_id");
   auto byte_array = actual.number.extract_as_byte_array();

   uint64_t random_int = 0;
   for (int i = 0; i < 8; i++)
   {
      random_int <<= 8;
      random_int |= (uint64_t)byte_array[i];
   }
   uint64_t r_result = random_int % max_value;
   return 1 + static_cast<uint8_t>(r_result);
}

/*========================================================================================================================
* ========================================================================================================================
*                  xRANDOM
* ========================================================================================================================
  =======================================================================================================================*/

ACTION daov::initrandom(name owner, uint64_t customer_id, uint64_t signing_value)
{
   require_auth(_self);
   action(
       {_self, "active"_n},
       "orng.wax"_n,
       "requestrand"_n,
       tuple<uint64_t, uint64_t, name>(customer_id, signing_value, _self))
       .send();
}

ACTION daov::getrandom(name owner, uint64_t customer_id)
{
   auto &actual = rand.get(customer_id, "invialid customer_id");
   uint64_t signing_value = static_cast<uint64_t>(current_time_point().sec_since_epoch()) + owner.value + customer_id;
   action({_self, "active"_n},
          "orng.wax"_n,
          "requestrand"_n,
          tuple<uint64_t, uint64_t, name>(actual.id, signing_value, _self))
       .send();
}

ACTION daov::receiverand(uint64_t customer_id, const checksum256 &random_value)
{

   require_auth("orng.wax"_n);

   auto itrCustomer = rand.find(customer_id);
   // if not, insert a new record
   if (itrCustomer == rand.end())
   {
      rand.emplace(_self, [&](auto &rec)
                   {
                      rec.id = customer_id;
                      rec.number = random_value; });
   }
   else
      rand.modify(itrCustomer, _self, [&](auto &rec)
                  { rec.number = random_value; });
}

/*========================================================================================================================
* ========================================================================================================================
*                  xTEMPLES
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::settemple(uint64_t id, string type, string name, uint8_t level, asset reward, string img)
// {
//    require_auth(_self);

//    auto temple = temples.find(id);

//    if (temple != temples.end())
//    {
//       temples.modify(temple, _self, [&](auto &rec)
//                      {
//                         rec.id = id;
//                         rec.type = type;
//                         rec.name = name;
//                         rec.level = level;
//                         rec.reward = reward;
//                         rec.img = img; });
//    }
//    else
//    {
//       temples.emplace(_self, [&](auto &rec)
//                       {
//                          rec.id = id;
//                          rec.type = type;
//                          rec.name = name;
//                          rec.level = level;
//                          rec.reward = reward;
//                          rec.img = img; });
//    }
// }

// ACTION daov::rmtemple(uint64_t id)
// {
//    require_auth(_self);

//    auto temple = temples.get(id, "No temple with this id");

//    temples.erase(temples.find(id));
// }

// ACTION daov::settempleimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto temple = temples.find(id);

//    check(temple != temples.end(), "No temple with this id");

//    temples.modify(temple, _self, [&](auto &rec)
//                   { rec.img = img; });
// }

void daov::onTemple(name owner, uint64_t temple_id)
{
   auto temple = temples.find(temple_id);

   auto user = accounts.find(owner.value);
   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   actions_u.emplace(owner, [&](auto &rec)
                     {
      rec.id = act_id;
      rec.completed = false;
      rec.owner = owner;
      rec.map_id = user->position;
      rec.action_name.push_back("TEMPLE");
      rec.action_price.push_back(temple->reward); });

   accounts.modify(user, owner, [&](auto &rec)
                   { rec.balances[0] += temple->reward; });
}

/*========================================================================================================================
* ========================================================================================================================
*                  xTOOLS
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::settool(uint64_t id, string type, string name, uint8_t level, asset price, asset reward, string img)
// {
//    require_auth(_self);

//    auto tool = tools.find(id);

//    if (tool != tools.end())
//    {
//       tools.modify(tool, _self, [&](auto &rec)
//                    {
//                       rec.id = id;
//                       rec.type = type;
//                       rec.name = name;
//                       rec.level = level;
//                       rec.price = price;
//                       rec.reward = reward;
//                       rec.img = img; });
//    }
//    else
//    {
//       tools.emplace(_self, [&](auto &rec)
//                     {
//                        rec.id = id;
//                        rec.type = type;
//                        rec.name = name;
//                        rec.level = level;
//                        rec.price = price;
//                        rec.reward = reward;
//                        rec.img = img; });
//    }
// }

// ACTION daov::rmtool(uint64_t id)
// {
//    require_auth(_self);

//    auto tool = tools.get(id, "No tool with this id");

//    tools.erase(tools.find(id));
// }

// ACTION daov::settoolimg(uint64_t id, string img)
// {
//    require_auth(_self);

//    auto tool = tools.find(id);

//    check(tool != tools.end(), "No tool with this id");

//    tools.modify(tool, _self, [&](auto &rec)
//                 { rec.img = img; });
// }

void daov::onTool(name owner)
{
   auto user = accounts.find(owner.value);

   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   auto tool = tools.find(map.find(user->position)->type_id);

   bool owned = false;

   for (auto it = user->tools.begin(); it != user->tools.end(); it++)
   {
      if (*it == tool->id)
      {
         owned = true;
         break;
      }
   }

   if (owned)
   {
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.owner = owner;
         rec.completed = true;
         rec.map_id = user->position;

         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
   }
   else
   {
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = user->position;

         rec.action_name.push_back("BUY");
         rec.action_price.push_back(tool->price);
         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
      accounts.modify(user, owner, [&](auto &rec)
                      { rec.action_id = act_id; });
   }
}

/*========================================================================================================================
* ========================================================================================================================
*                  xWHEEL
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setwheel(uint64_t id, string type, asset value)
// {
//    require_auth(_self);

//    auto part = wheel.find(id);

//    if (part != wheel.end())
//    {
//       wheel.modify(part, _self, [&](auto &rec)
//                    {
//                       rec.id = id;
//                       rec.type = type;
//                       rec.value = value; });
//    }
//    else
//    {
//       wheel.emplace(_self, [&](auto &rec)
//                     {
//                        rec.id = id;
//                        rec.type = type;
//                        rec.value = value; });
//    }
// }
// ACTION daov::rmwheel(uint64_t id)
// {
//    require_auth(_self);

//    auto part = wheel.get(id, "No land with this id");

//    wheel.erase(wheel.find(id));
// }

/*========================================================================================================================
* ========================================================================================================================
*                  xACTION
* ========================================================================================================================
  =======================================================================================================================*/

ACTION daov::doaction(name owner, uint8_t choice)
{
   require_auth(owner);

   string exist_type[] = {"tools", "lands", "energies", "buildings"};

   auto user = accounts.find(owner.value);
   actions_t actions_u(_self, owner.value);

   check(user != accounts.end(), "User not logged");

   auto action = actions_u.find(user->action_id);
   check(action != actions_u.end(), "No exisiting action with this id");

   check(owner == action->owner, "This is not your action");

   check(action->completed == false, "Already done action");

   bool buy = false;
   bool exit_jail = false;

   // if user don't have any money or the action is free, just skip it
   // todo handle quizz response
   accounts.modify(user, owner, [&](auto &rec)
                   { rec.action_id = 0; });
   if (user->balances[0].amount == 0 || action->action_price[choice].amount == 0)
   {
      actions_u.modify(action, owner, [&](auto &rec)
                       {
         if(action->action_price[choice].amount){
         rec.action_name.clear();
         rec.action_name.push_back("SKIP");
         rec.action_price.clear();
         rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
         rec.completed = true;
         rec.player_to_pay.clear();
         rec.price_for_each.clear();}
         else{
            if (rec.action_name[0].rfind("QUIZZ ", 0) == 0) {
               uint64_t quizz_value = stoi(rec.action_name[0].substr(6));
               check(choice > 1 && choice < 6, "Please make a valide choice");
               rec.action_name.clear();
               rec.action_price.clear();
               rec.action_name.push_back("QUIZZ "+to_string(quizz_value));
               rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
               testquizz(owner, quizz_value, choice);
            }
            else {

               if (rec.action_name.size() > 1) {
               rec.action_name.erase(choice ? rec.action_name.begin() : ++(rec.action_name.begin()));
               rec.action_price.erase(choice ? rec.action_price.begin() : ++(rec.action_price.begin()));
               }
               if (rec.action_name[0] == "RETRY" || rec.action_name[0] == "WHEEL") {
               launchWheel(owner);
                }
               else if (rec.action_name[0] == "SWITCH") {
                  accounts.modify(user, owner, [&](auto &rec) {
                     rec.position = map.find(rec.position)->type_id;
                  });
                  onNeverlucky(owner);
               }
         }
         } });
   }
   else
   {
      // if user have enought money to pay
      if (user->balances[0].amount >= action->action_price[choice].amount)
      {
         actions_u.modify(action, owner, [&](auto &rec)
                          {
         if (rec.action_name.size() > 1)
            rec.action_name.erase(choice ? rec.action_name.begin() : ++(rec.action_name.begin()));
         if (rec.action_price.size() > 1)
         rec.action_price.erase(choice ? rec.action_price.begin() : ++(rec.action_price.begin()));
         rec.completed = true;
         if (rec.action_name[0] == "PAY" || rec.action_name[1] == "PAY") {
            //pay owners
            for (auto i = 0; i < rec.player_to_pay.size(); i++) {
               if (i == 1)
                  break ;
               auto player = accounts.find(rec.player_to_pay[i].value);
               if (player != accounts.end()) {
                  accounts.modify(player, _self, [&](auto &record) {
                     record.balances[0] += rec.price_for_each[i];
                     
                  });
               }
            }
            getcase(owner, false);
            
         }
         else if (rec.action_name[0] == "EXIT") {
            exit_jail = true;
         }
         else if (rec.action_name[0] == "BUY") {
            buy = true;
         } });
         // spend money
         accounts.modify(user, owner, [&](auto &rec)
                         {
            rec.balances[0] -= action->action_price[0];
            
            if (exit_jail)
            rec.jailed = 0;
            if (buy) {
               //handle if he buy a boost
               auto buy_case = map.find(rec.position);
               
               if (buy_case->case_type == "boosts") {
                  rec.boosts[buy_case->type_id] = getNbOfBoost(owner, exist_type[buy_case->type_id]) + 1;
                  buy = false;
               }
               else {
                  //get the id of boost to check if he have it
                  uint8_t boost_id = getBoostType(buy_case->case_type);
                  
                  //if it's a tool, add it direct here
                  if (buy_case->case_type == "tools") {
                     rec.tools.push_back(buy_case->type_id);
                     buy = false;
                  }
                  //if he have te boost, count number of ownership and add the new one (+2 because new one + boost card)
                  if (rec.boosts[boost_id])
                  {
                     rec.boosts[boost_id] = getNbOfBoost(owner, buy_case->case_type) + 2;
                  }
               }
            } });
         if (buy)
         {
            handleBuy(owner, map.find(action->map_id)->case_type, map.find(action->map_id)->type_id);
         }
      }
      else
      {
         check(action->action_name[choice] != "BUY", "Insufficient DAOV....");
         actions_u.modify(action, owner, [&](auto &rec)
                          {
         if (rec.action_name.size() > 1)       
            rec.action_name.erase(choice ? rec.action_name.begin() : rec.action_name.cbegin());
         if (rec.action_price.size() > 1) 
            rec.action_price.erase(choice ? rec.action_price.begin() : rec.action_price.cbegin());
         rec.completed = true;
         for (auto i = rec.player_to_pay.size() - 1; i >= 0; i--) {
            auto player = accounts.find(rec.player_to_pay[i].value);
            if (player != accounts.end()) {
               rec.price_for_each[i].amount = rec.price_for_each[i].amount * user->balances[0].amount / rec.action_price[0].amount;
               accounts.modify(player, _self, [&](auto &record) {
                  record.balances[0].amount += rec.price_for_each[i].amount;
               });
            }
         }
         rec.action_price[0] = user->balances[0]; });
         accounts.modify(user, owner, [&](auto &rec)
                         {
            rec.balances[0].amount = 0;
            if (exit_jail)
            rec.jailed = 0; });
      }
   }
}

void daov::handleBuy(name owner, string type, uint64_t id)
{
   switch (str2int(type.c_str()))
   {
   case str2int("lands"):
        buyLand(owner, id);
        break;
   case str2int("buildings"):
        buyBuilding(owner, id);
        break;
   case str2int("energies"):
        buyEnergy(owner, id);
        break;
    case str2int("companies"):
        buyCompany(owner, id);
        break;
   default:
      break;
   }
}

void daov::buyLand(name owner, uint64_t id)
{
   auto land = lands.find(id);

   lands.modify(land, owner, [&](auto &rec)
                { rec.owners.push_back(owner); });
}

void daov::buyBuilding(name owner, uint64_t id)
{
   auto building = buildings.find(id);

   buildings.modify(building, owner, [&](auto &rec)
                    { rec.owners.push_back(owner); });
}

void daov::buyEnergy(name owner, uint64_t id)
{
   auto energy = energies.find(id);

   energies.modify(energy, owner, [&](auto &rec)
                   { rec.owners.push_back(owner); });
}

void daov::buyCompany(name owner, uint64_t id)
{
    auto company = companies.find(id);

    companies.modify(company, owner, [&](auto &rec) { rec.owners.push_back(owner); });
}

/*========================================================================================================================
* ========================================================================================================================
*                  xBOOST
* ========================================================================================================================
  =======================================================================================================================*/

uint64_t daov::getNbOfBoost(name owner, string type)
{
   uint64_t nb = 0;
   switch (str2int(type.c_str()))
   {
   case str2int("lands"):
      for (auto it = lands.begin(); it != lands.end(); it++)
      {
         for (auto user = it->owners.begin(); user != it->owners.end(); user++)
         {
            if (*user == owner)
            {
               nb++;
            }
         }
      }
      break;
   case str2int("buildings"):
      for (auto it = buildings.begin(); it != buildings.end(); it++)
      {
         for (auto user = it->owners.begin(); user != it->owners.end(); user++)
         {
            if (*user == owner)
            {
               nb++;
            }
         }
      }
      break;
   case str2int("energies"):
      for (auto it = energies.begin(); it != energies.end(); it++)
      {
         for (auto user = it->owners.begin(); user != it->owners.end(); user++)
         {
            if (*user == owner)
            {
               nb++;
            }
         }
      }
      break;
   default:
      break;
   }
   return nb;
}

void daov::onBoost(name owner)
{
   auto user = accounts.find(owner.value);

   actions_t actions_u(_self, owner.value);
   uint64_t act_id = actions_u.available_primary_key();

   auto boost = boosts.find(map.find(user->position)->type_id);

   bool owned = false;

   if (user->boosts[boost->id] > 0)
      owned = true;

   if (owned)
   {
      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = true;
         rec.owner = owner;
         rec.map_id = user->position;

         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
   }
   else
   {

      actions_u.emplace(owner, [&](auto &rec)
                        {
         rec.id = act_id;
         rec.completed = false;
         rec.owner = owner;
         rec.map_id = user->position;

         rec.action_name.push_back("BUY");
         rec.action_price.push_back(boost->price);
         rec.action_name.push_back("SKIP");
         rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
      accounts.modify(user, owner, [&](auto &rec)
                      { rec.action_id = act_id; });
   }
}

/*========================================================================================================================
* ========================================================================================================================
*                  xSWITCH
* ========================================================================================================================
  =======================================================================================================================*/

// Switch case, user need to chose between SWITCH to "Neverlucky" case or SKIP
void daov::onSwitch(name owner)
{
   auto user = accounts.find(owner.value);

   actions_t actions_u(_self, owner.value);
   auto act_id = actions_u.available_primary_key();

   // Create uaction
   actions_u.emplace(owner, [&](auto &rec)
   {
      rec.id = act_id;
      rec.completed = false;
      rec.owner = owner;
      rec.map_id = user->position;

      rec.action_name.push_back("SWITCH");
      rec.action_name.push_back("SKIP");
      rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
      rec.action_price.push_back(asset(0, symbol("DAOV", 0))); 

      accounts.modify(user, owner, [&](auto &rec) { rec.action_id = act_id; });
   });

   // Fill user's account with new action
   accounts.modify(user, owner, [&](auto &rec)
                   { rec.action_id = act_id; });
}

ACTION daov::rmactionuser(name owner)
{
   require_auth(_self);

   auto user = accounts.find(owner.value);

   check(user != accounts.end(), "unknown account");

   accounts.modify(user, _self, [&](auto &rec)
                   { rec.action_id = 0; });
}

// ACTION daov::givedaov(name owner, asset value)
// {
//    require_auth(_self);

//    auto user = accounts.find(owner.value);

//    check(user != accounts.end(), "unknown account");

//    accounts.modify(user, _self, [&](auto &rec)
//                    { rec.balances[0] += value; });
// }

/*========================================================================================================================
* ========================================================================================================================
*                  xCONTRIBS
* ========================================================================================================================
  =======================================================================================================================*/

void daov::onContribs(name owner)
{
   auto user = accounts.find(owner.value);

   auto contrib = contribs.find(map.find(user->position)->type_id);

   asset price = (user->balances[0] >= contrib->cost ? contrib->cost : user->balances[0]);
   actions_t actions_u(_self, owner.value);

   accounts.modify(user, owner, [&](auto &rec)
                   { rec.balances[0] -= price; });

   if (bank.begin() == bank.end())
   {
      bank.emplace(owner, [&](auto &rec)
                   {
         rec.id = 1;
         rec.giveaway = price; });
   }
   else
   {
      bank.modify(bank.begin(), owner, [&](auto &rec)
                  { rec.giveaway += price; });
   }

   actions_u.emplace(owner, [&](auto &rec)
                     {
      rec.id = actions_u.available_primary_key();
      rec.completed = false;
      rec.owner = owner;
         rec.map_id = user->position;

         rec.action_name.push_back("CONTRIBS");
         rec.action_price.push_back(price); });
}

/*========================================================================================================================
* ========================================================================================================================
*                  xCOMPANIES
* ========================================================================================================================
  =======================================================================================================================*/

// Company function, user can select between PAY fees or BUY the company
void daov::onCompany(name owner)
{
    auto user = accounts.find(owner.value);
    auto tile = map.find(user->position);

    auto price = 0;
    auto owned = false;
    auto company = user->position == 15 ? companies.find(1) : companies.find(2);

    // Gestion for Agricultural Company
    if (user->position == 15)
    {   
        // Calculate buildings price
        for (auto it = buildings.begin(); it != buildings.end(); it++)
            for (auto i = it->owners.begin(); i != it->owners.end(); i++)
                if (*i == owner)
                    price += 30;

        // Calculate energies price
        for (auto it = energies.begin(); it != energies.end(); it++)
            for (auto i = it->owners.begin(); i != it->owners.end(); i++)
                if (*i == owner)
                    price += 20;

        if (price == 0)
            price = 10; 
    }
    else if (user->position == 23)
    {
        // Find if user get the tile
        for (auto it = company->owners.begin(); it != company->owners.end(); it++)
            if (*it == owner)
                owned = true;
        
        // Calculate price with addition of launches
        price = (user->last_launch[0] + user->last_launch[1]) * 15;
    }

    // Find if user get the tile
    for (auto it = company->owners.begin(); it != company->owners.end(); it++)
        if (*it == owner)
            owned = true;

    // Create new uactions
    actions_t actions_u(_self, owner.value);
    uint64_t act_id = actions_u.available_primary_key();
    actions_u.emplace(owner, [&](auto &rec)
    {
        rec.id = act_id;
        rec.completed = false;
        rec.owner = owner;
        rec.map_id = user->position;
        if (!owned)
        {
            rec.action_name.push_back("BUY");
            rec.action_price.push_back(company->price);
        }

        rec.action_name.push_back("PAY");
        rec.action_price.push_back(asset(price, symbol("DAOV", 0)));
        for (auto it = company->owners.begin(); it != company->owners.end(); it++)
            rec.player_to_pay.push_back(*it);
        rec.price_for_each.push_back(asset(price / company->owners.size(), symbol("DAOV", 0)));
        accounts.modify(user, owner, [&](auto &rec) { rec.action_id = act_id; });
    });
}

/*========================================================================================================================
* ========================================================================================================================
*                  xNEVERLUCKY
* ========================================================================================================================
  =======================================================================================================================*/

// ACTION daov::setnl(uint64_t id, string desc, asset reward, int16_t move)
// {
//    require_auth(_self);

//    auto nl = neverlucky.find(id);

//    if (nl != neverlucky.end())
//    {
//       neverlucky.modify(nl, _self, [&](auto &rec)
//                         {
//                        rec.id = id;
//                        rec.desc = desc;
//                        rec.reward = reward;
//                        rec.move = move; });
//    }
//    else
//    {
//       neverlucky.emplace(_self, [&](auto &rec)
//                          {
//                         rec.id = id;
//                        rec.desc = desc;
//                        rec.reward = reward;
//                        rec.move = move; });
//    }
// }

void daov::onNeverlucky(name owner)
{
   uint64_t i = 0;
   for (auto it = neverlucky.begin(); it != neverlucky.end(); it++)
   {
      i++;
   }
   uint64_t max_value = i;
   auto &actual = rand.get(owner.to_string().size() % 2 ? 77777777777777 : 88888888888888, "invialid customer_id");
   auto byte_array = actual.number.extract_as_byte_array();

   uint64_t random_int = 0;
   for (int i = 0; i < 8; i++)
   {
      random_int <<= 8;
      random_int |= (uint64_t)byte_array[i];
   }
   uint64_t r_result = (random_int % max_value) + 1;
   actions_t actions_u(_self, owner.value);

   auto user = accounts.find(owner.value);
   auto nl = neverlucky.find(r_result);

   check(nl != neverlucky.end(), "This neverlucky does not exist.");

   accounts.modify(user, owner, [&](auto &rec)
                   {
      rec.balances[0] += nl->reward;
      rec.position += nl->move; });

   actions_u.emplace(owner, [&](auto &rec)
                     {
         
      rec.id = actions_u.available_primary_key();
      rec.completed = false;
      rec.owner = owner;
      rec.map_id = user->position;
      rec.action_name.push_back("NEVERLUCKY");
      rec.action_price.push_back(nl->reward); });

   accounts.modify(user, owner, [&](auto &rec)
                   {
      if (rec.position > 40)
      {
         rec.position -= 40;
         rec.balances[0] += conf.begin()->reward_start;
      }
      else if (rec.position == 31)
      {
      rec.position = 11;
      rec.action_id = actions_u.available_primary_key() ;
      rec.jailed = 1;
      } });

   if (nl->move)
   {
      if (!user->jailed)
      {
         getcase(owner, true);
      }
      else
      {
         actions_u.emplace(owner, [&](auto &rec)
                           {
            rec.id = user->action_id;
            rec.completed = false;
            rec.owner = owner;
            rec.map_id = 11;
            rec.action_name.push_back("EXIT");
            rec.action_price.push_back(conf.begin()->exit_jail_price);
            rec.action_name.push_back("SKIP");
            rec.action_price.push_back(asset(0, symbol("DAOV", 0))); });
      }
   }
}

string to_uppers(string str)
{
   string res = "";
   string swap = str;
   for (auto &c : str)
   {
      c = toupper(c);
   }
   res = str;
   str = swap;
   return res;
}

void daov::launchWheel(name owner)
{
   uint64_t i = 0;
   for (auto it = wheel.begin(); it != wheel.end(); it++)
   {
      i++;
   }
   uint64_t max_value = i;
   actions_t actions_u(_self, owner.value);

   auto user = accounts.find(owner.value);

   auto &actual = rand.get(owner.to_string().size() % 2 ? 77777777777777 : 88888888888888, "invialid customer_id");
   auto byte_array = actual.number.extract_as_byte_array();

   uint64_t random_int = 0;
   for (int i = 0; i < 8; i++)
   {
      random_int <<= 8;
      random_int |= (uint64_t)byte_array[i];
   }
   uint64_t r_result = (random_int % max_value);

   auto nl = wheel.find(r_result);

   check(nl != wheel.end(), "This wheel does not exist.");

   auto act_id = actions_u.available_primary_key();

   bool retry = nl->type == "retry" ? true : false;
   bool giveaway = nl->type == "giveaway" ? true : false;
   auto bank_u = bank.begin();

   actions_u.emplace(owner, [&](auto &rec)
                     {
      rec.id = act_id;
      rec.owner = owner;
      rec.map_id = user->position;
      rec.completed = !retry;
      
      rec.action_name.push_back(to_uppers(nl->type));
      rec.action_price.push_back(giveaway ? bank_u->giveaway : nl->value); });
   accounts.modify(user, owner, [&](auto &rec)
                   { rec.balances[1] += nl->value;
                   if (rec.balances[1].amount < 0)
                     rec.balances[1].amount = 0;
                     if (retry){
                        rec.action_id = act_id;
                     }
                     if (giveaway) {
                        rec.balances[0] += bank_u->giveaway;
                     } });
   if (giveaway)
   {
      bank.modify(bank_u, owner, [&](auto &rec)
                  { rec.giveaway = asset(0, symbol("DAOV", 0)); });
   }
}

void daov::onWheel(name owner)
{

   auto user = accounts.find(owner.value);
   actions_t actions_u(_self, owner.value);
   auto act_id = actions_u.available_primary_key();

   accounts.modify(user, owner, [&](auto &rec)
                   { rec.action_id = act_id; });

   actions_u.emplace(owner, [&](auto &rec)
                     {
      rec.id = act_id;
      rec.completed = false;
      rec.owner = owner;
      rec.action_name.push_back("WHEEL");
      rec.action_name.push_back("SKIP");
      rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
      rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
      rec.map_id = user->position; });
}

void daov::onQuizz(name owner)
{
   auto user = accounts.find(owner.value);
   actions_t action_u(_self, owner.value);

   auto act_id = action_u.available_primary_key();

   uint64_t i = 0;
   for (auto it = quizz.begin(); it != quizz.end(); it++)
   {
      i++;
   }
   uint64_t max_value = i;
   actions_t actions_u(_self, owner.value);

   

   auto &actual = rand.get(owner.to_string().size() % 2 ? 77777777777777 : 88888888888888, "invialid customer_id");
   auto byte_array = actual.number.extract_as_byte_array();

   uint64_t random_int = 0;
   for (int i = 0; i < 8; i++)
   {
      random_int <<= 8;
      random_int |= (uint64_t)byte_array[i];
   }
   uint64_t r_result = (random_int % max_value);

   auto place = quizz.find(r_result);

   accounts.modify(user, owner, [&](auto &rec)
                   { rec.action_id = act_id; });

   action_u.emplace(owner, [&](auto &rec)
                    {
      rec.id = act_id;
      rec.owner = owner;
      rec.map_id = user->position;
      rec.completed = false;
      rec.action_name.push_back("QUIZZ " + to_string(r_result));
      rec.action_name.push_back(place->question);
      rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
      rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
      for (auto it = place->answers.begin(); it != place->answers.end(); it++) {
         rec.action_name.push_back(*it);
         rec.action_price.push_back(asset(0, symbol("DAOV", 0)));
      } });
}

ACTION daov::setquizz(uint64_t id, string question, vector<string> answers, checksum256 valid)
{
   require_auth(_self);
   auto place = quizz.find(id);

   if (place != quizz.end())
   {
      quizz.modify(place, _self, [&](auto &rec)
                   {
      rec.id = id;
      rec.question = question;
      rec.answers = answers;
      rec.valid = valid; });
   }
   else
   {
      quizz.emplace(_self, [&](auto &rec)
                    {
      rec.id = id;
      rec.question = question;
      rec.answers = answers;
      rec.valid = valid; });
   }
}

void daov::testquizz(name owner, uint64_t id, int8_t answer)
{
   auto place = quizz.find(id);
   auto user = accounts.find(owner.value);
   actions_t action_u(_self, owner.value);

   auto act_id = action_u.available_primary_key();

   string response = "thallard=" + to_string(answer) + "=thjacques";
   checksum256 resp256 = sha256(response.c_str(), response.size());

   if (resp256 == place->valid)
   {
      accounts.modify(user, owner, [&](auto &rec)
                      { rec.balances[0].amount += 50; });
      action_u.emplace(owner, [&](auto &rec)
                       {
            rec.id = act_id;
            rec.owner = owner;
            rec.map_id = user->position;
            rec.action_name.push_back("VALID");
            rec.action_price.push_back(asset(50, symbol("DAOV", 0)));
            rec.completed = true; });
   }
   else
   {
      accounts.modify(user, owner, [&](auto &rec)
                      {
            rec.balances[0].amount -= 25;
            if (rec.balances[0].amount < 0)
               rec.balances[0].amount = 0; });
      action_u.emplace(owner, [&](auto &rec)
                       {
            rec.id = act_id;
            rec.owner = owner;
            rec.map_id = user->position;
            rec.action_name.push_back("FAILED");
            rec.action_price.push_back(asset(-25, symbol("DAOV", 0)));
            rec.completed = true; });
   }
}